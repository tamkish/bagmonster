#define SHOULD_CRASH


using System;
using System.Linq;
using Godot;

namespace BagMonster.GodotUtils;
//todo get rid of, didnt use 
// https://github.com/godotengine/godot-proposals/issues/2425#issuecomment-841597854
// code by DevinPentecost
public class OnReadyGetNode : Attribute
{
    public OnReadyGetNode(string nodePath/*, bool crash*/)
    {
        NodePath = nodePath;
        /*Crash = crash;*/
    }

    public string NodePath { get; }
    /*
    public bool Crash { get; }
*/
}

public static class OnReadyGetNodeExtensions
{
    public static void GetOnReadyNodes<T>(this T node)
    {
        if (node is not Node converted)
        {
            var message = $"Object {typeof(T)} is not a superclass of {typeof(Node)}. Unable to assign OnReady Nodes";

#if SHOULD_CRASH
            OS.Alert(message);
#else
            GD.PrintErr(message);
#endif


            return;
        }

        var annotatedFields = typeof(T).GetFields()
            .Where(info => info.IsDefined(typeof(OnReadyGetNode), false));
        foreach (var annotatedField in annotatedFields)
        {
            //Retrieve the NodePath
            var nodePath = annotatedField.GetCustomAttributes(typeof(OnReadyGetNode), false).Cast<OnReadyGetNode>()
                .First().NodePath;
            var targetNode = converted?.GetNode(nodePath);

            //If we couldn't get anything, let the user know
            if (targetNode is null)
            {
                var message = ($"Unable to find Node at path {nodePath}");

#if SHOULD_CRASH
                OS.Alert(message);
#else
            GD.PrintErr(message);
#endif
                continue;
            }

            try
            {
                //Set the field
                annotatedField.SetValue(node, targetNode);
            }
            catch (ArgumentException exception)
            {
                //The node we found was of the wrong type
                var message = $"Found Node at path {nodePath}, but was of the incorrect type. {exception.Message}";

#if SHOULD_CRASH
                OS.Alert(message);
#else
            GD.PrintErr(message);
#endif
            }
        }
    }
}