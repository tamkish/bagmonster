using Godot;

namespace BagMonster;

public static class Area2DExtensions
{
    public static bool IsInteractorArea(this Area2D self)
    {
        return self.IsInGroup("InteractorArea");
    }
}