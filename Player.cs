using System;
using System.Reflection;
using BagMonster.enums;
using BagMonster.GodotUtils;
using Godot;
using Godot.Collections;

namespace BagMonster;

public partial class Player : CharacterBody2D
{
    private Sprite2D _sprite = null!;

    private Texture2D _upTexture = null!;
    private Texture2D _downTexture = null!;
    private Texture2D _leftTexture = null!;
    private Texture2D _rightTexture = null!;

    private bool IsMoving => !Velocity.IsZeroApprox();

    public override void _Ready()
    {
        _sprite = GetNode<Sprite2D>("Sprite");
        _upTexture = ResourceLoader.Load<Texture2D>("res://assets/sprites/player/up/player_up_0.png");
        _downTexture = ResourceLoader.Load<Texture2D>("res://assets/sprites/player/down/player_down_0.png");
        _leftTexture = ResourceLoader.Load<Texture2D>("res://assets/sprites/player/left/player_left_0.png");
        _rightTexture = ResourceLoader.Load<Texture2D>("res://assets/sprites/player/right/player_right_0.png");
    }

    public override void _Process(double delta)
    {
        if (IsMoving)
        {
            //update sprite
            var isHorizontal = Mathf.Abs(Velocity.X) > Mathf.Abs(Velocity.Y);

            if (isHorizontal)
            {
                var isLeft = Velocity.X < 0;
                ChangeSprite(isLeft ? Cardinal.Left : Cardinal.Right);
            }
            else
            {
                var isUp = Velocity.Y < 0;
                ChangeSprite(isUp ? Cardinal.Up : Cardinal.Down);
            }
        }
    }

    private void ChangeSprite(Cardinal direction)
    {
        _sprite.Texture = direction switch
            {
                Cardinal.Up => _upTexture,
                Cardinal.Down => _downTexture,
                Cardinal.Left => _leftTexture,
                Cardinal.Right => _rightTexture,
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
            };
    }

    // private void AdvanceAnimation() { throw new System.NotImplementedException(); }

    public override void _PhysicsProcess(double delta)
    {
        #region inputVel

        var inputVel = Vector2.Zero;

        if (Input.IsActionPressed("move_down"))
        {
            inputVel += Vector2.Down;
        }

        if (Input.IsActionPressed("move_left"))
        {
            inputVel += Vector2.Left;
        }

        if (Input.IsActionPressed("move_up"))
        {
            inputVel += Vector2.Up;
        }

        if (Input.IsActionPressed("move_right"))
        {
            inputVel += Vector2.Right;
        }

        inputVel = inputVel.Normalized();

        #endregion

        Velocity = inputVel;
        Velocity *= 70;
        MoveAndSlide();
        Position = Position.SnappedToInt();


        if (Input.IsActionJustPressed("ui_accept"))
        {
            GD.Print(Position.X, Position.Y);
        }
    }
}
/*
var vel : Vector2 = Vector2()
var facingDir : Vector2 = Vector2()

onready var dialogue = get_node("/root/Root_Node/CanvasLayer/panel")
onready var save = get_node("/root/Root_Node/CanvasLayer/savePanel")
onready var menu = get_node("/root/Root_Node/CanvasLayer/infoPanel")

func _ready():
    self.set_position(SceneChanger._playerPosition) #After transitioning to a new scene go to the preset position for that transition mask

func _physics_process (delta):

    vel = Vector2()

    # inputs
    var isMoving : bool = false
    if !dialogue.is_visible() && !save.is_visible() && !menu.is_visible():
        var isDiagonal: bool = false
        if Input.is_action_pressed("move_up"):
            vel.y -= 1
            facingDir = Vector2(0, -1)
            isMoving = true
        if Input.is_action_pressed("move_down"):
            vel.y += 1
            facingDir = Vector2(0, 1)
            isMoving = true
        if Input.is_action_pressed("move_left"):
            vel.x -= 1
            facingDir = Vector2(-1, 0)
            isMoving = true
        if Input.is_action_pressed("move_right"):
            vel.x += 1
            facingDir = Vector2(1, 0)
            isMoving = true

        # Direction Conflict Handling
        if Input.is_action_pressed("move_up") && Input.is_action_pressed("move_down") && vel.x == 1: #player velocity is right
            vel.y -= 1
            facingDir = Vector2(1, 0)
            isMoving = true
        elif Input.is_action_pressed("move_up") && Input.is_action_pressed("move_down") && vel.x == -1: #player velocity is left
            vel.y -= 1
            facingDir = Vector2(-1, 0)
            isMoving = true
        elif Input.is_action_pressed("move_up") && Input.is_action_pressed("move_down"):
            vel.y -= 1
            facingDir = Vector2(0, -1)
            isMoving = true

        if Input.is_action_pressed("move_left") && Input.is_action_pressed("move_right") && vel.y == 1: #player velocity is down
            vel.x -= 1
            facingDir = Vector2(0, 1) #face up
            isMoving = true
        elif Input.is_action_pressed("move_left") && Input.is_action_pressed("move_right") && vel.y == -1: #player velocity is up
            vel.x -= 1
            facingDir = Vector2(0, -1) #face down
            isMoving = true
        elif Input.is_action_pressed("move_left") && Input.is_action_pressed("move_right"):
            vel.x -= 1
            facingDir = Vector2(-1, 0)
            isMoving = true


        if isMoving:
            var currentAnim = $AnimationPlayer.current_animation
            if Input.is_action_pressed("move_up") && Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_down") && !Input.is_action_pressed("move_right"): #Up Left
                isDiagonal = true
                if currentAnim != "Left" and currentAnim != "Up":
                    $AnimationPlayer.play("Up")
                if $AnimationPlayer.current_animation == "Up":
                    $AnimationPlayer.play("Up")
                if $AnimationPlayer.current_animation == "Left":
                    $AnimationPlayer.play("Left")
            if Input.is_action_pressed("move_up") && Input.is_action_pressed("move_right") && !Input.is_action_pressed("move_down") && !Input.is_action_pressed("move_left"): #Up Right
                isDiagonal = true
                if currentAnim != "Right" and currentAnim != "Up":
                    $AnimationPlayer.play("Up")
                if $AnimationPlayer.current_animation == "Up":
                    $AnimationPlayer.play("Up")
                if $AnimationPlayer.current_animation == "Right":
                    $AnimationPlayer.play("Right")
            if Input.is_action_pressed("move_down") && Input.is_action_pressed("move_left") && !Input.is_action_pressed("move_up") && !Input.is_action_pressed("move_right"): #Down Left
                isDiagonal = true
                if currentAnim != "Down" and currentAnim != "Left":
                    $AnimationPlayer.play("Down")
                if $AnimationPlayer.current_animation == "Down":
                    $AnimationPlayer.play("Down")
                if $AnimationPlayer.current_animation == "Left":
                    $AnimationPlayer.play("Left")
            if Input.is_action_pressed("move_down") && Input.is_action_pressed("move_right") && !Input.is_action_pressed("move_up") && !Input.is_action_pressed("move_left"): #Down Right
                isDiagonal = true
                if currentAnim != "Down" and currentAnim != "Right":
                    $AnimationPlayer.play("Down")
                if $AnimationPlayer.current_animation == "Down":
                    $AnimationPlayer.play("Down")
                if $AnimationPlayer.current_animation == "Right":
                    $AnimationPlayer.play("Right")
            if !isDiagonal:
                if facingDir == Vector2(0, -1): #Up
                    if $AnimationPlayer.current_animation != "Up":
                        $AnimationPlayer.play("Up")
                if facingDir == Vector2(0, 1): #Down
                    if $AnimationPlayer.current_animation != "Down":
                        $AnimationPlayer.play("Down")
                if facingDir == Vector2(-1, 0): #Left
                    if $AnimationPlayer.current_animation != "Left":
                        $AnimationPlayer.play("Left")
                if facingDir == Vector2(1, 0): #Right
                    if $AnimationPlayer.current_animation != "Right":
                        $AnimationPlayer.play("Right")
        #normalize the velocity to prevent faster diagonal movement
        vel = vel.normalized()

        # move the player
        move_and_slide(vel * moveSpeed)

    # stop walk animation when standing still
    if(!isMoving):
        $AnimationPlayer.stop()
        $AnimationPlayer.seek(0.8, true)
 *
 */