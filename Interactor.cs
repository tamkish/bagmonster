using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public partial class Interactor : Node2D
{
    private static Interactor? _singleton;

    public static Interactor Singleton
    {
        get
        {
            if (_singleton == null)
            {
                GD.PushError("Interactor is null. someone tried to access it");
            }

            Debug.Assert(_singleton != null, nameof(_singleton) + " != null");
            return _singleton;
        }
    }


    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        if (_singleton != null)
        {
            GD.PushError("Interactor has been already assigned. there shouldve been only one? ");
        }

        _singleton = this;
    }


    private List<InteractTarget> _targets = new();

    public void SubscribeTarget(InteractTarget target)
    {
        if (!_targets.Contains(target))
        {
            _targets.Add(target);
        }
    }

    public void UnsubscribeTarget(InteractTarget target)
    {
        if (_targets.Contains(target))
        {
            _targets.Remove(target);
        }

        else
        {
            GD.PushError(
                "interact target trying to unsubscribe itself from the interactor. the _targets doesnt have it though.",
                target,
                _targets);
        }
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(double delta)
    {
        if (Input.IsActionJustPressed("interact"))
        {
            PerformInteraction();
        }
    }

    void PerformInteraction()
    {
        if (_targets.Count == 0)
        {
            return;
        }

        //todo decide which one 
        var selectedTarget = _targets.First();

        

    }
}