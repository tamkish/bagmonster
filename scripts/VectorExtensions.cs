using System.Diagnostics.Contracts;
using Godot;

namespace BagMonster;

public static class VectorExtensions
{
    [Pure]
    public static Vector2 SnappedToInt(this Vector2 original)
    {
        var newX = Mathf.RoundToInt( original.X);
        var newY = Mathf.RoundToInt( original.Y);
        return new Vector2(newX, newY);
    }
}