namespace BagMonster.enums;

public enum Cardinal
{
    Up,
    Down,
    Left,
    Right
}